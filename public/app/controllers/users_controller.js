FoorumApp.controller('UsersController', function($scope, $location, Api){
	// Toteuta kontrolleri tähän
	$scope.register = function(){
		Api.register({username: $scope.newUsername, password: $scope.newPassword1})
		.success(function(user){
			console.log('Rekisteröityminen onnistui!');
			console.log(user);
			$location.path('/');
		})
		.error(function(){
			console.log('Rekisteröityminen epäonnistui! Lisätään käyttäjälle virheilmoitus');
			$scope.errorMessage = "Rekisteröityminen epäonnistui"; 
		});
	};
	
	$scope.login = function(){
		Api.login({username: $scope.username, password: $scope.password})
			.success(function(user){
				console.log('Kirjautuminen onnistui!');
				console.log(user);
				$scope.user = user;
				$location.path('/');
			})
			.error(function(){
				console.log('Kirjautuminen epäonnistui! Lisätään käyttäjälle virheilmoitus');
				$scope.errorMessage = 'Väärä käyttäjätunnus tai salasana!';
			});
	};
});