FoorumApp.controller('ShowMessageController', function($scope, $routeParams, Api){
	// Toteuta kontrolleri tähän
	Api.getMessage($routeParams.id).success(function(message){
		$scope.message = message;
		$scope.replies = message.Replies;
		$scope.replyAmount = message.Replies.length;
	});
	$scope.addReply = function(){
		console.log("Painettiin reply lisäystä");
		Api.addReply($scope.newReply, $scope.message.id).success(function(reply){
			$scope.replies.push(reply);
		});
	};
});
